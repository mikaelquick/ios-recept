//
//  ViewController.swift
//  recept
//
//  Created by Mikael  Quick on 2017-09-09.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase
import FirebaseStorage

class ReceptCell: UITableViewCell{
    
    @IBOutlet weak var receptName: UILabel!
    @IBOutlet weak var receptPicture: UIImageView!
}

class Recept: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var myTableview: UITableView!
    @IBOutlet var popUp: UIView!
    
    var snapshoto : DataSnapshot?
    var databaseRef : DatabaseReference!
    var storage = Storage.storage()
    
    var uid = UIDevice.current.identifierForVendor!.uuidString
    var clickAble = false
    
    var images = [UIImage]()
    var image = UIImage()
    var names = [String]()
    var name = String()
    var ingredArray = [String]()
    var how2Cook = String()
    var allReceptCat : NSDictionary = [:]
  
    override func viewDidLoad() {
        super.viewDidLoad()
        allReceptCat = (snapshoto?.value as? NSDictionary)!
    }
    
    @objc func receptTapped(tapGestureRecognizer: UITapGestureRecognizer){
        
        if clickAble{
            let receptCell = (tapGestureRecognizer.view) as! ReceptCell
            name = receptCell.receptName.text!
            image = receptCell.receptPicture.image!
            how2Cook = snapshoto?.childSnapshot(forPath:name).childSnapshot(forPath: "How2Cook").value as! String

            if let tempDic = self.snapshoto?.childSnapshot(forPath: name).childSnapshot(forPath: "Ingredients").value as? [String:AnyObject]{
            
            for each in tempDic{
                ingredArray.append("\(each.key)")
            }
        }
          else{
            print("error")
        }
        performSegue(withIdentifier: "details", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "details"{
            
            let webView = segue.destination as! Details
            webView.name = name
            webView.How2Cook = how2Cook
            webView.ingredArray = ingredArray
            webView.image = image
            ingredArray = [String]()
    }
    
    }
    
    //TableView
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action , indexPath) -> Void in
            
            let storageRef = self.storage.reference()
            let tempimageRef = storageRef.child(self.uid).child("\(self.names[indexPath.row]).png")
            
            tempimageRef.delete { error in
                if error != nil {
                    // Coulnt Delete
                } else {
                    // Deleted
                }
            }
            
            self.databaseRef.child(self.names[indexPath.row]).removeValue()
            self.performSegue(withIdentifier: "goHome", sender: self)
            
        })
        
        deleteAction.backgroundColor = UIColor.red
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allReceptCat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tempName = allReceptCat.allKeys[indexPath.row] as? String
        names.append(tempName!)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReceptCell
        
        let link =  snapshoto?.childSnapshot(forPath: names[indexPath.row]).childSnapshot(forPath: "Link").value as! String
        
        let gsReference = storage.reference(forURL: link)
        
        gsReference.getData(maxSize: 1 * 1024 * 1024){ data, error in
            if error != nil {
                self.navigationController?.popViewController(animated: true)
                
            } else {
                self.images.append(UIImage(data: data!)!)
                cell.receptPicture.image = UIImage(data: data!)
                if self.images.count == self.names.count{
                    self.clickAble = true
                }
            }}
        
        
        cell.receptName.text = names[indexPath.row]
        cell.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(receptTapped(tapGestureRecognizer:)))
        cell.addGestureRecognizer(tapGestureRecognizer)
        
        return cell
    }


}
