//
//  AddView.swift
//  recept
//
//  Created by Mikael  Quick on 2017-09-11.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase
import FirebaseStorage

class ingredTableview: UITableViewCell{
    @IBOutlet weak var ingredName: UILabel!
    @IBOutlet weak var ingredBackground: UIView!
}

class AddRecept: UIViewController, UIImagePickerControllerDelegate , UINavigationControllerDelegate,  UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var ingredTable: UITableView!
    @IBOutlet weak var peaceSignImage: UIImageView!
    @IBOutlet weak var botText: UILabel!
    @IBOutlet weak var topText: UILabel!
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var catInput: UITextField!
    @IBOutlet weak var ingredInput: UITextField!
    @IBOutlet weak var how2CookInput: UITextView!
    @IBOutlet weak var peaceSign: UIView!

    @IBOutlet weak var ingredTableHeight: NSLayoutConstraint!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    
    var refrenseDatabase : DatabaseReference!
    var tempimageRef : StorageReference!
    var metaData : StorageMetadata!

    var ingredientsDic = [ String : String]()
    var ingredients = [String]()
    var myImage : UIImage?
    var uid = UIDevice.current.identifierForVendor!.uuidString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(peaceSignTapped(tapGestureRecognizer:)))
        peaceSign.addGestureRecognizer(tapGestureRecognizer)
        refrenseDatabase = Database.database().reference()
        
        let addButton = UIBarButtonItem.init(barButtonSystemItem: .add,
                                             target: self,
                                             action: #selector(addRecept))
        addButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = addButton
        
        how2CookInput.layer.borderWidth = 0.5
    }
    
    @objc func addRecept(){
        
        if(!nameInput.text!.isEmpty && !catInput.text!.isEmpty && !how2CookInput.text!.isEmpty && ingredientsDic.count > 0){
            
            let cat = catInput.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let name = nameInput.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let how2 = how2CookInput.text!
            
            let storage = Storage.storage()
            let storageRef = storage.reference()
            metaData = StorageMetadata()
            
            if(myImage != nil){
                metaData.contentType = "image/png"
                tempimageRef = storageRef.child(uid).child(cat).child("\(name).png")
                myImage = myImage!.resizedTo1MB()
                tempimageRef.putData(UIImagePNGRepresentation(myImage!)!, metadata: metaData)
                refrenseDatabase.child(uid).child(cat).child(name).child("Link").setValue("\(tempimageRef!)")
            }
            else{
                refrenseDatabase.child(uid).child(cat).child(name).child("Link").setValue("gs://recept-e99c8.appspot.com/noPicture.jpg")
            }
            
            refrenseDatabase.child(uid).child(cat).child(name).child("Ingredients").setValue(ingredientsDic)
            refrenseDatabase.child(uid).child(cat).child(name).child("How2Cook").setValue(how2)
            
            performSegue(withIdentifier: "goHome", sender: self)
        }
        else{
            addAlertDialog(message: "Fill in all the textfields", title: "Warning")
        }
    }
    
    @IBAction func addIngredient(_ sender: Any) {
        if !ingredInput.text!.isEmpty{
            ingredientsDic["\(ingredInput.text!)"] = "0"
            ingredients.append("\(ingredInput.text!)")
            ingredTableHeight.constant += 54
            ingredTable.reloadData()
            mainViewHeight.constant += 54
            ingredInput.text = ""
        }
        else{
            addAlertDialog(message: "Your ingredients textfield is empty", title: "Warning")
    }
    }
    
    func addAlertDialog(message : String, title : String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action) in alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ingredTableview
        cell.ingredName.text = ingredients[indexPath.row]
        return cell
    }

    @IBAction func addPicture(_ sender: Any) {
        let image = UIImagePickerController()
        image.sourceType = .photoLibrary
        image.delegate = self
        present(image, animated: false, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            peaceSignImage.contentMode = .scaleAspectFill
            peaceSignImage.image = pickedImage
            myImage = pickedImage
            
            topText.text = "Picture"
            botText.text = "chosen"
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func peaceSignTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let image = UIImagePickerController()
        image.sourceType = .photoLibrary
        image.delegate = self
        present(image, animated: false, completion: nil)
    }
}


