//
//  DetailController.swift
//  recept
//
//  Created by Mikael  Quick on 2017-09-11.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit

class DetailTableCell: UITableViewCell{
    
    @IBOutlet weak var ingredLabel: UILabel!
}

class Details: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var ingredLabel: UILabel!
    
    @IBOutlet weak var howToCookView: UIView!
    @IBOutlet weak var ingredView: UIView!
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var how2CookLabel: UILabel!
    @IBOutlet var ingredInput: UITextView!
    @IBOutlet var resultLabel: UILabel!
    @IBOutlet var how2CookTextview: UITextView!
    
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var image = UIImage()
    var ingred = String()
    var name = String()
    var ingredArray = [String]()
    var How2Cook = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = name
       how2CookTextview.text = How2Cook
        
        let rows = CGFloat(ingredArray.count * 54)
        tableViewHeight.constant = CGFloat(rows)
        mainViewHeight.constant += rows
        dishImage.image = image
        
        //Borders
        ingredView.layer.borderWidth = 0.5
        howToCookView.layer.borderWidth = 0.5
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetailTableCell
        cell.tag = indexPath.row
        cell.ingredLabel.text = ingredArray[indexPath.row]
        
        return cell
    }
}
