//
//  CategoryController.swift
//  recept
//
//  Created by Mikael  Quick on 2017-09-18.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage

class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var catLabel: UILabel!
}

class Main: UIViewController , UITableViewDelegate, UITableViewDataSource {

    var databaseRef : DatabaseReference!
    var snapShot : DataSnapshot?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    
    var uid = UIDevice.current.identifierForVendor!.uuidString
    var categorys = [String]()
    var chosenCat : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //Database
        databaseRef = Database.database().reference()
        getData()
        
        //AddButton & Hiding BackButton
        self.navigationItem.setHidesBackButton(true, animated: false)
        let addButton = UIBarButtonItem.init(barButtonSystemItem: .add,
                                             target: self,
                                             action: #selector(addRecept))
        addButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    @objc func addRecept(){
        performSegue(withIdentifier: "addRecept", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "recepts"{
            let webView = segue.destination as! Recept
            webView.snapshoto = self.snapShot?.childSnapshot(forPath: chosenCat)
            webView.databaseRef = databaseRef.child(uid).child(chosenCat)
        }
    }
    
    @objc func categoryTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let number = tapGestureRecognizer.view?.tag
        chosenCat = categorys[number!]
        performSegue(withIdentifier: "recepts", sender: self)
    }
    
    //Database
    func getData(){
        self.databaseRef.child(self.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            self.snapShot = snapshot
            
            if let tempDic = snapshot.value as? [String:AnyObject]{
                
                for each in tempDic{
                    self.categorys.append(each.key)
                    self.mainViewHeight.constant += 70
                    DispatchQueue.main.async{
                        self.tableView.reloadData()
                    }
                }
            }
        })
    }
    
    //TableView
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action , indexPath) -> Void in

            let storage = Storage.storage()
            let storageRef = storage.reference()
            
            if let tempDic = self.snapShot?.childSnapshot(forPath: self.categorys[indexPath.row]).value as? [String:AnyObject]{
                
                for each in tempDic{
                    print(each.key)
                    let imageRef = storageRef.child(self.uid).child(self.categorys[indexPath.row]).child("\(each.key).png")
                    imageRef.delete { error in
                        if error != nil {
                            //print("couln't delete")
                        } else {
                            //print("delete")
                        }
                    }
                }
            }
            
            self.databaseRef.child(self.uid).child(self.categorys[indexPath.row]).removeValue()
            self.categorys.removeAll()
            self.mainViewHeight.constant = 450
            self.getData()
            self.tableView.reloadData()
        })
        
        deleteAction.backgroundColor = UIColor.red
        
        return [deleteAction]
    }
    
    func deleteCategory(){
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categorys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(categoryTapped(tapGestureRecognizer:)))
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CategoryTableViewCell
        cell.isUserInteractionEnabled = true

        cell.addGestureRecognizer(tapGestureRecognizer)
        cell.tag = indexPath.row
        cell.catLabel.text = categorys[indexPath.row]
        
        return cell
    }
}

//Extensions
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIImage {
    
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resizedTo1MB() -> UIImage? {
        guard let imageData = UIImagePNGRepresentation(self) else { return nil }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / 1024.0
        
        while imageSizeKB > 1024 {
            guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
                let imageData = UIImagePNGRepresentation(resizingImage)
                else { return nil }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / 1024.0
        }
        
        return resizingImage
    }
}
